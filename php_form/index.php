<style>
    *{margin:0 auto; width:95%}
    .form-group{
        width:100%; padding:5px; float:left;
    }
    .heading{
        width:160px; font-size:16px; margin:10px; float:left;
    }
    .data{
        width:400px; padding:7px;float:left;
    }
    .btn{ width: 80px; padding:5px; background:#f34f3d; float:right;}
</style>
<form method="post" action="action.php">

    <div class="form-group">
        <label class="heading" for="name">Name:</label>
        <input class="data" type="text" name="name" id="name" placeholder="Input your name"/>

        <label class="heading" for="email">Email:</label>
        <input class="data" type="text" name="email" id="email" placeholder="Input your email"/>
    </div>
    <div class="form-group">
        <label class="heading" for="mobile">Mobile:</label>
        <input class="data" type="text" name="mobile" id="mobile" placeholder="Input your mobile no"/>

        <label class="heading" for="dob">Date of Birth:</label>
        <input class="data"  type="text" name="dob" id="dob" placeholder="dd/mm/yyyy"/>
    </div>
    <div class="form-group">
        <label class="heading" for="nid">National ID No.:</label>
        <input class="data" type="text" name="nid" id="nid" placeholder="13 or 17 digit integer"/>

        <label class="heading" for="district">Home District:</label>
        <input class="data" type="text" name="district" id="district" placeholder="Input your district"/>
    </div>
    <div class="form-group">
        <label class="heading" for="address1">Address-1:</label>
        <input class="data" type="text" name="address1" id="address1" placeholder="Input your address"/>

        <label class="heading" for="address2">Address-2:</label>
        <input class="data" type="text" name="address2" id="address2" placeholder="Input your address2"/>
    </div>
    <input class="btn" type="submit" name="" Value="Submit"/>
    <input class="btn" type="reset" name="" Value="Reset"/>

</form>

